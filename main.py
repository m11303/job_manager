from datetime import datetime, timedelta
import json
import threading
import time
import traceback
import logging

import requests
import uvicorn
from fastapi import FastAPI
from pottery import Redlock
from pydantic import BaseModel

import job_db
import redis_queue

RUNNING = False


class Module(BaseModel):
    module: str
    config: dict


class Result(BaseModel):
    job_id: int
    status: str
    result: bool
    agent_id: int = None


def get_queue():
    queue = redis_queue.get_queue()
    logging.info(queue)
    non_prio_queue = [q for q in queue if not q.get('prio')]
    prio_queue = [q for q in queue if q.get('prio')]
    prio_queue.extend(non_prio_queue)
    return prio_queue


def calculate_timeout(config, job):
    if config.get('timeoutpr'):
        timeout = int(config.get('timeout')) * 60 * len(job.data[config.get('timeoutpr')])
    else:
        timeout = int(config.get('timeout')) * 60
    return timeout


def start_queue(sleep):
    global RUNNING

    if sleep:
        time.sleep(sleep)
    if RUNNING:
        return
    RUNNING = True

    lock = Redlock(key='queue', masters={redis_queue.r}, auto_release_time=60)
    if lock.acquire():
        try:
            queue = get_queue()

            for item in queue:
                if item.get('schedule') and int(item.get('schedule')) > datetime.now().timestamp():
                    start(int(item.get('schedule')) - int(datetime.now().timestamp()))
                    continue
                config = job_db.get_module(item.get('module_name')).config

                agent_data = {}
                if config.get('driver'):
                    agent = job_db.agent_lease(config.agent, config.module, calculate_timeout(config, item))
                    agent_data['agent_id'] = agent.id
                    agent_data['agent_path'] = agent.path

                job = job_db.get_job(item.get('job_id'))
                job.status = "Started"
                job_db.update_job(job)

                post = {"job_id": item.get('job_id'), "agent": agent_data, "data": item.get('data')}
                requests.post(f"http://{item.get('module_name')}:8001/run/", json=post)

                redis_queue.r.lrem("queue", 0, json.dumps(item))

                if item.get('recurrent'):
                    item['schedule'] = datetime.now().timestamp() + timedelta(hours=24).total_seconds()
                    redis_queue.queue(item)
        except:
            logging.error(traceback.format_exc())
        finally:
            RUNNING = False
            lock.release()


app = FastAPI()


@app.get("/im_alive")
async def get_job(module: Module):
    logging.info(f"adding modul to db: {module.module}")
    job_db.update_module(module)


@app.get("/job_done")
async def job_done(result: Result):
    if result.agent_id:
        job_db.agent_release(result.agent_id)

    job = job_db.get_job(result.job_id)
    job.status = result.status
    job.result = result.result
    job_db.update_job(job)


def start(sleep=0):
    if not RUNNING:
        threading.Thread(target=start_queue, args=[sleep]).start()


@app.get("/wake_queue")
async def wake_queue():
    start()


@app.get("/healthy")
def healthy():
    return {'msg': "im good"}


if __name__ == '__main__':
    start(sleep=5)
    uvicorn.run("main:app", host="0.0.0.0", port=5001)
