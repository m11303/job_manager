import json
import os

import redis

r = redis.Redis(host=os.environ["redis_host"], port=6379, db=0)


def queue(item: dict):
    r.rpush("queue", json.dumps(item))


def get_queue() -> list[json]:
    return [json.loads(q) for q in r.lrange("queue", 0, r.llen("queue"))]


def delete_queue_element(data: dict):
    r.lrem("queue", 0, json.dumps(data))


def delete_queue():
    for key in r.scan_iter("*"):
        r.delete(key)
