FROM python:3.10-slim

RUN apt-get update
WORKDIR /app
RUN mkdir "result_share"
RUN mkdir "temp"
COPY requirements.txt .
RUN apt-get install -y g++
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY . .

ENTRYPOINT [ "python" ]
CMD [ "main.py" ]
