import os
from datetime import datetime, timedelta
import json
import random
from typing import List, Optional

import sqlalchemy
from sqlalchemy import Column, Integer, String, DateTime, JSON, BOOLEAN
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from sqlalchemy.orm import relationship, Session

# an Engine, which the Session will use for connection
# resources
engine = create_engine(f'postgresql://test:testpass@{os.environ["host"]}:5432/db', echo=True, future=True, pool_pre_ping=True)
# engine = create_engine('postgresql://test:testpass@host.docker.internal:5432/db', echo=True, future=True)


Base = declarative_base()


class Job(Base):
    __tablename__ = 'job'
    id = Column(Integer, primary_key=True)
    owner = Column(String, nullable=False)
    module = Column(String, nullable=False)
    status = Column(String, nullable=False)
    group = Column(String, nullable=False)
    logs = relationship("Log", back_populates="job")
    target = Column(String)
    data = Column(JSON)
    prio = Column(BOOLEAN)
    recurrent = Column(BOOLEAN)
    result = Column(BOOLEAN)
    timestamp = Column(DateTime)


class Agent(Base):
    __tablename__ = 'agent'
    id = Column(Integer, primary_key=True)
    path = Column(String, nullable=False)
    name = Column(String, nullable=False)
    group = Column(String, nullable=False)
    exclusive = Column(BOOLEAN, nullable=False)
    description = Column(String)
    blocked = Column(BOOLEAN)
    blocked_by = Column(String)
    blocked_to = Column(DateTime)


class Modules(Base):
    __tablename__ = 'modules'
    id = Column(Integer, primary_key=True)
    module_name = Column(String, nullable=False)
    config = Column(JSON, nullable=False)


class Log(Base):
    __tablename__ = 'logs'
    id = Column(Integer, primary_key=True)
    job = relationship("Job", back_populates="logs")
    job_id = Column(Integer, ForeignKey('job.id'))
    module = Column(String)
    timestamp = Column(DateTime)  # the current timestamp
    logger = Column(String)  # the name of the logger. (e.g. myapp.views)
    level = Column(String)  # info, debug, or error?
    trace = Column(String)  # the full traceback printout
    msg = Column(String)  # any custom log you may have included

    def __repr__(self):
        return f"<Log: {self.timestamp} - {self.msg[:50]}>"


# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)


def get_job(job_id: int) -> Job:
    with Session(engine) as s:
        job = s.get(Job, job_id)
        return job


def get_job_by_group(group: str) -> List[Job]:
    with Session(engine) as s:
        jobs = s.execute(select(Job).filter_by(group=group)).scalars()
        return [job for job in jobs]


def add_job(owner: str, module: str, group: str, data: dict, target: str, recurrent: int = 0, prio: bool = False):
    with Session(engine) as s:
        job = Job(owner=owner, module=module, group=group, status="queued", data=json.loads(json.dumps(data)),
                  recurrent=recurrent, prio=prio, timestamp=datetime.datetime.now(), result=False, target=target)
        s.add(job)
        s.commit()
        return job.id


def all_log() -> List[Log]:
    with Session(engine) as s:
        logs = s.execute(select(Log)).scalars()
        return [log for log in logs]


def job_log(job_id: int) -> List[Log]:
    with Session(engine) as s:
        logs = s.execute(select(Log).filter_by(job_id=job_id)).scalars()
        return [log for log in logs]


def get_log(log_id: int) -> Log:
    with Session(engine) as s:
        log = s.get(Log, log_id)
        return log


def delete_job(job_id: int):
    with Session(engine) as s:
        job = s.get(Job, job_id)
        s.delete(job)
        s.commit()


def get_all_jobs() -> List[Job]:
    with Session(engine) as s:
        jobs = s.execute(select(Job)).scalars()
        return [job for job in jobs]


def update_module(module):
    with Session(engine) as s:
        try:
            db_module = s.execute(select(Modules).filter_by(module_name=module.module)).scalar_one()
            db_module.config = module.config
        except sqlalchemy.exc.NoResultFound:
            db_module = Modules(module_name=module.module, config=module.config)
        s.add(db_module)
        s.commit()


def get_module(module_name) -> Optional[Modules]:
    with Session(engine) as s:
        try:
            return s.execute(select(Modules).filter_by(module_name=module_name)).scalar_one()
        except sqlalchemy.exc.NoResultFound:
            return None


def agent_lease(agent_group: str, module: str, timeout: int) -> Optional[Agent]:
    with Session(engine) as s:
        agents = s.execute(select(Agent).filter_by(group=agent_group)).scalars()
        random.shuffle(agents)
        for agent in agents:
            if not agent.exclusive:
                return agent

            if not agent.blocked or agent.blocked_to < datetime.now():
                agent.blocked = True
                agent.blocked_by = module
                agent.blocked_to = datetime.now() + timedelta(minutes=timeout)
                s.add(agent)
                s.commit()
                return agent

        return None


def agent_release(agent_id: int):
    with Session(engine) as s:
        agent = s.execute(select(Agent).filter_by(id=agent_id)).scalar_one()
        agent.blocked = False
        agent.blocked_by = ""
        agent.blocked_to = None
        s.add(agent)
        s.commit()


def update_job(job):
    with Session(engine) as s:
        s.add(job)
        s.commit()
